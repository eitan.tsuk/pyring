v2.0.1: new time-axis, cleanup of data reading/generation.
v2.1.0: add TEOB-PM and massively parallel CPNest.
v2.2.1: add quadratic modes and tails.
